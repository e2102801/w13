import logo from './logo.svg';
import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ];
  const arrayOfOptionWeekdays = weekdays.map((weekday, index)=>{
    return (
    <tr key={index} value={index}>
      <td className='border'>{weekday}</td>
    </tr>
    );
  })
  return (
    <div className="App">
      <table>
        <thead>
          <tr>
            <th>Weekdays</th>
          </tr>
        </thead>
        <tdata>{arrayOfOptionWeekdays}</tdata>
      </table>
    </div>
  );
}

export default App;
